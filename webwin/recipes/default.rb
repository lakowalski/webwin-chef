#
# Cookbook:: webwin
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

site 'test.insysgo.com' do
	port 80
	action :create
end

deployment 'test.insysgo.com' do
	source 'https://s3-eu-west-1.amazonaws.com/www-demo.lab.insysgo.com/_deploy/cmi-cloud.insysgo.com.zip'
end

