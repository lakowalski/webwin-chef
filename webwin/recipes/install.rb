#
# Cookbook:: webwin
# Recipe:: default
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

include_recipe 'iis'

# install iis server with components
iis_install 'iis' do
	additional_components ['IIS-ManagementConsole', 'IIS-ASPNET45', 'TelnetClient']
	action :install
end

# stop and delete the default site
iis_site 'Default Web Site' do
  action [:stop, :delete]
end

iis_pool 'DefaultAppPool' do
        action :stop
end

# install urlrewrite
include_recipe 'iis_urlrewrite'

# create deployment directory
directory "#{node['webwin']['deployroot']}" do
  action :create
end

