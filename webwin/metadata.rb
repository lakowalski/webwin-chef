name 'webwin'
maintainer 'The Authors'
maintainer_email 'you@example.com'
license 'All Rights Reserved'
description 'Installs/Configures webwin'
long_description 'Installs/Configures webwin'
version '0.1.0'
chef_version '>= 12.0'

supports    "windows"

depends "iis"
depends "iis_urlrewrite"
