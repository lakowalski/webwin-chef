resource_name :deployment

property :site_name, String, name_property: true
property :source, String, required: true

action :deploy do
 	remote_file "#{node['webwin']['deployroot']}/#{new_resource.site_name}.zip" do
		source new_resource.source
		action :create
  	end

	windows_zipfile "#{node['iis']['docroot']}\\#{new_resource.site_name}" do
		source "#{node['webwin']['deployroot']}/#{new_resource.site_name}.zip"
		action :unzip
  	end

	file "#{node['webwin']['deployroot']}/#{new_resource.site_name}.zip" do
		action :delete
	end	
end
