#
# Cookbook:: webwin
# Resource:: site
#
# Copyright:: 2019, The Authors, All Rights Reserved.
#

resource_name :site

property :site_name, String, name_property: true
property :port, Integer, required: true

action :create do
	iis_pool new_resource.site_name do
		runtime_version "4.0"
		pipeline_mode :Integrated
		action :add
	end

	directory "#{node['iis']['docroot']}\\#{new_resource.site_name}" do
  		action :create
	end

	directory "#{node['iis']['pubroot']}\\logs\\#{new_resource.site_name}" do
		action :create
	end

	iis_site new_resource.site_name do
		application_pool new_resource.site_name
 		protocol :http
		port new_resource.port
		path "#{node['iis']['docroot']}/#{new_resource.site_name}"
		log_directory "#{node['iis']['pubroot']}/logs/#{new_resource.site_name}"
  		action [:add,:start]
	end

end

action :delete do
        iis_site new_resource.site_name do
                action [:stop,:delete]
        end

        iis_pool new_resource.site_name do
                action [:stop,:delete]
        end


	directory "#{node['iis']['docroot']}\\#{new_resource.site_name}" do
		recursive true
		action :delete
        end

        directory "#{node['iis']['pubroot']}\\logs\\#{new_resource.site_name}" do
		recursive true
		action :delete
        end
end
